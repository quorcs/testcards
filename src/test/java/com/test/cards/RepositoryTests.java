package com.test.cards;

import com.test.cards.entity.Application;
import com.test.cards.entity.RegisterOrder;
import com.test.cards.entity.RegisterUser;
import com.test.cards.repository.ApplicationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RepositoryTests {

	@Autowired
	private ApplicationRepository applicationRepository;

	@Test
	public void saveRegisterUserTest() {
		RegisterUser registerUser = new RegisterUser("user", "pass");
		RegisterUser registerUserResult = applicationRepository.save(registerUser);

		assertThat(registerUser).isEqualTo(registerUserResult);
	}

	@Test
	public void saveRegisterOrderTest() {
		RegisterOrder registerOrder = new RegisterOrder(new Timestamp(System.currentTimeMillis()), 10);
		RegisterOrder registerOrderResult = applicationRepository.save(registerOrder);

		assertThat(registerOrder).isEqualTo(registerOrderResult);
	}

	@Test
	public void findApplicationByIdTest() {
		RegisterOrder registerOrder = new RegisterOrder(new Timestamp(System.currentTimeMillis()), 10);
		RegisterUser registerUser = new RegisterUser("user", "pass");

		applicationRepository.save(registerOrder);
		applicationRepository.save(registerUser);

		RegisterUser registerUserResult = (RegisterUser) applicationRepository.findApplicationById(2L);

		assertThat(registerUser).isEqualTo(registerUserResult);
	}

	@Test
	public void findAllApplicationsTest() {
		RegisterOrder registerOrder = new RegisterOrder(new Timestamp(System.currentTimeMillis()), 10);
		RegisterUser registerUser = new RegisterUser("user", "pass");

		applicationRepository.save(registerOrder);
		applicationRepository.save(registerUser);

		Set<Application> applications = new HashSet<>();
		applications.add(registerOrder);
		applications.add(registerUser);

		Set<Application> applicationsResult = applicationRepository.findAll();

		assertThat(applications).isEqualTo(applicationsResult);
	}
}
