package com.test.cards;

import com.test.cards.controller.ApplicationController;
import com.test.cards.dto.RequestDTO;
import com.test.cards.entity.Application;
import com.test.cards.entity.RegisterUser;
import com.test.cards.service.LogicService;
import com.test.cards.util.JsonUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ApplicationController.class)
public class RestTests {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8"));

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private LogicService logicService;

	@Test
	public void addApplicationTest() throws Exception {
		RequestDTO requestDTO = new RequestDTO();
		requestDTO.setLogin("user");
		requestDTO.setPassword("pass");
		requestDTO.setType("user");

		MvcResult result = mockMvc.perform(
				post("/application")
						.contentType(contentType)
						.content(JsonUtil.toJson(requestDTO))
						.accept(MediaType.APPLICATION_JSON))
				.andReturn();

		int resultStatus = result.getResponse().getStatus();

		assertThat(HttpStatus.OK.value()).isEqualTo(resultStatus);
	}

	@Test
	public void getApplicationByIdTest() throws Exception {
		RegisterUser registerUser = new RegisterUser(1L, "user", "pass");

		when(logicService.getApplicationById(1L)).thenReturn(registerUser);

		mockMvc.perform(
				get("/application/getById")
						.contentType(contentType)
						.param("id", "1")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(1)))
				.andExpect(jsonPath("$.login", is("user")))
				.andExpect(jsonPath("$.password", is("pass")));
	}

	@Test
	public void getAllApplicationsTest() throws Exception {
		RegisterUser registerUser = new RegisterUser(1L, "admin", "admin");

		Set<Application> applications = new HashSet<>();
		applications.add(registerUser);

		when(logicService.getAllApplications()).thenReturn(applications);

		mockMvc.perform(
				get("/application/getAll")
						.contentType(contentType)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].login", is("admin")))
				.andExpect(jsonPath("$[0].password", is("admin")));
	}
}
