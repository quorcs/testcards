package com.test.cards.rabbitmq;

import com.test.cards.dto.RequestDTO;
import com.test.cards.entity.Application;
import com.test.cards.factory.ApplicationFactory;
import com.test.cards.service.LogicService;
import com.test.cards.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class RabbitMQReceiver {

	@Autowired
	private LogicService logicService;

	/**
	 * Получение и обработка сообщения из очереди RabbitMQ
	 * @param s сообщение из очереди
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public void receiveMessage(String s) throws InterruptedException, IOException {
		RequestDTO requestDTO = JsonUtil.fromJson(s);
		String type = requestDTO.getType();
		Application application = logicService
				.saveApplication(ApplicationFactory.getApplicationByType(type), requestDTO);
		logicService.process(application);
	}
}
