package com.test.cards.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * DTO-класс полученного запроса
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestDTO {

	@NotNull
	@ApiModelProperty(value = "Type of application")
	private String type;

	@ApiModelProperty(value = "Date of order create for register order")
	private Timestamp date;

	@ApiModelProperty(value = "Count of order's positions for register order")
	private Integer positions;

	@ApiModelProperty(value = "User login for register user")
	private String login;

	@ApiModelProperty(value = "User password for register user")
	private String password;
}
