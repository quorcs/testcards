package com.test.cards.service;

import com.test.cards.dto.RequestDTO;
import com.test.cards.entity.Application;

import java.util.Set;

public interface ApplicationService {

	Set<Application> getAllApplications();

	Application getApplicationById(Long id);

	Application saveApplication(Application application, RequestDTO requestDTO);
}
