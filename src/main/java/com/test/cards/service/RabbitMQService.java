package com.test.cards.service;

import com.test.cards.dto.RequestDTO;

import java.io.IOException;

public interface RabbitMQService {

	void addApplicationToQueue(RequestDTO requestDTO) throws IOException;
}
