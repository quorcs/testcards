package com.test.cards.service;

import com.test.cards.dto.RequestDTO;
import com.test.cards.entity.Application;

import java.io.IOException;
import java.util.Set;

public interface LogicService {

	Set<Application> getAllApplications();

	Application getApplicationById(Long id);

	Application saveApplication(Application application, RequestDTO requestDTO);

	void addApplicationToQueue(RequestDTO requestDTO) throws IOException;

	Application process(Application application) throws InterruptedException;
}
