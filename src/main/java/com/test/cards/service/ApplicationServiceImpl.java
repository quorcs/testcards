package com.test.cards.service;

import com.test.cards.converter.ApplicationConverter;
import com.test.cards.dto.RequestDTO;
import com.test.cards.entity.Application;
import com.test.cards.repository.ApplicationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class ApplicationServiceImpl implements ApplicationService {

	private final ApplicationRepository applicationRepository;

	/**
	 * Получение всех заявок
	 * @return Applications
	 */
	@Override
	public Set<Application> getAllApplications() {
		return applicationRepository.findAll();
	}

	/**
	 * Получение заявки по идентификатору
	 * @param id идентификатор
	 * @return Application, либо IllegalArgumentException в случае отсутствия заявки по заданному id
	 */
	@Override
	public Application getApplicationById(Long id) {
		return Optional
				.ofNullable(applicationRepository.findApplicationById(id))
				.orElseThrow(IllegalArgumentException::new);
	}

	/**
	 * Сохранения заявки
	 * @param application заявка
	 * @param requestDTO класс-DTO полученного запроса
	 * @return Application
	 */
	@Override
	public Application saveApplication(Application application, RequestDTO requestDTO) {
		return Optional
				.ofNullable(application)
				.map(m -> applicationRepository.save(ApplicationConverter.buildApplication(application, requestDTO)))
				.orElse(null);
	}
}
