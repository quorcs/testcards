package com.test.cards.service;

import com.test.cards.dto.RequestDTO;
import com.test.cards.entity.Application;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class LogicServiceImpl implements LogicService {

	private final ApplicationService applicationService;
	private final RabbitMQService rabbitMQService;

	/**
	 * Получение всех заявок
	 * @return Applications
	 */
	@Override
	public Set<Application> getAllApplications() {
		return applicationService.getAllApplications();
	}

	/**
	 * Получение заявки по идентификатору
	 * @param id идентификатор
	 * @return Application, либо IllegalArgumentException в случае отсутствия заявки с заданным id
	 */
	@Override
	public Application getApplicationById(Long id) {
		return applicationService.getApplicationById(id);
	}

	/**
	 * Сохранение заявки
	 * @param application заявка
	 * @param requestDTO класс-DTO полученного запроса
	 * @return Application
	 */
	@Override
	public Application saveApplication(Application application, RequestDTO requestDTO) {
		return applicationService.saveApplication(application, requestDTO);
	}

	/**
	 * Добавление заявки в очередь RabbitMQ
	 * @param requestDTO класс-DTO полученного запроса
	 * @throws IOException
	 */
	@Override
	public void addApplicationToQueue(RequestDTO requestDTO) throws IOException {
		rabbitMQService.addApplicationToQueue(requestDTO);
	}

	/**
	 * Обработка заявки
	 * @param application заявка
	 * @return заявка
	 * @throws InterruptedException
	 */
	@Override
	public Application process(Application application) throws InterruptedException {
		Thread.sleep(1000);
		return application;
	}
}
