package com.test.cards.service;

import com.test.cards.dto.RequestDTO;
import com.test.cards.util.JsonUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class RabbitMQServiceImpl implements RabbitMQService {

	private final RabbitTemplate rabbitTemplate;

	@Value("${rabbit.queue.name}")
	private String queueName;

	/**
	 * Асинхронный метод добавления заявки в очередь
	 * @param requestDTO DTO с полученными параметрами
	 * @throws IOException
	 */
	@Async
	@Override
	public void addApplicationToQueue(RequestDTO requestDTO) throws IOException {
		rabbitTemplate.convertAndSend(queueName, JsonUtil.toJson(requestDTO));
	}
}
