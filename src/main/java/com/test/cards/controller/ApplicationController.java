package com.test.cards.controller;

import com.test.cards.dto.RequestDTO;
import com.test.cards.entity.Application;
import com.test.cards.service.LogicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Set;

@RestController
@RequestMapping("/application")
@RequiredArgsConstructor
@Api(value = "application", description = "Main RESTs this service")
public class ApplicationController {

	private final LogicService logicService;

	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Adding an application to queue for processing", response = String.class)
	public ResponseEntity<?> addApplication(@Validated @RequestBody RequestDTO requestDTO) throws IOException {
		logicService.addApplicationToQueue(requestDTO);
		return new ResponseEntity<>("Application was added to queue!", HttpStatus.OK);
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	@ApiOperation(value = "View a list of all applications in database", response = Set.class)
	public ResponseEntity<Set<Application>> getAllApplications() {
		return new ResponseEntity<>(logicService.getAllApplications(), HttpStatus.OK);
	}

	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	@ApiOperation(value = "View the application for id or error if not exist.", response = Object.class)
	public ResponseEntity<?> getApplication(@RequestParam Long id) throws IllegalArgumentException {
		return new ResponseEntity<>(logicService.getApplicationById(id), HttpStatus.OK);
	}
}
