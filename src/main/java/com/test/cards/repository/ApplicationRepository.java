package com.test.cards.repository;

import com.test.cards.entity.Application;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ApplicationRepository extends CrudRepository<Application, Long> {

	Set<Application> findAll();

	Application findApplicationById(Long id);
}
