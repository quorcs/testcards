package com.test.cards.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "register_order")
public class RegisterOrder extends Application {
	private Timestamp date;
	private Integer positions;
}
