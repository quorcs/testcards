package com.test.cards.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "register_user")
public class RegisterUser extends Application {
	private String login;
	private String password;

	public RegisterUser(Long id, String login, String password) {
		super(id);
		this.login = login;
		this.password = password;
	}
}
