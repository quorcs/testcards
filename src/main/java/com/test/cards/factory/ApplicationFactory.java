package com.test.cards.factory;

import com.test.cards.entity.Application;
import com.test.cards.entity.RegisterOrder;
import com.test.cards.entity.RegisterUser;
import org.springframework.stereotype.Component;

@Component
public class ApplicationFactory {

	/**
	 * Получение нужного объекта по заданному типу
	 * @param applicationType тип объекта
	 * @return объект Application
	 */
	public static Application getApplicationByType(String applicationType) {
		switch (applicationType) {
			case "order":
				return new RegisterOrder();
			case "user":
				return new RegisterUser();
			default:
				return null;
		}
	}
}
