package com.test.cards.util;

import com.test.cards.dto.RequestDTO;
import lombok.NonNull;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;

public class JsonUtil {

	private static final ObjectMapper mapper = new ObjectMapper();

	/**
	 * Преобразование объекта в Json
	 * @param obj объект
	 * @return Json в формате String
	 */
	public static String toJson(@NonNull Object obj) throws IOException {
		ObjectWriter objectWriter = mapper.writer().withDefaultPrettyPrinter();

		return objectWriter.writeValueAsString(obj);
	}

	/**
	 * Преобразование из Json в объект
	 * @param string Json в формате String
	 * @return объект RequestDTO
	 */
	public static RequestDTO fromJson(String string) throws IOException {
		return mapper.readValue(string, new TypeReference<RequestDTO>(){});
	}
}
