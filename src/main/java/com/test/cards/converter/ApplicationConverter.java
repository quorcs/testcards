package com.test.cards.converter;

import com.test.cards.dto.RequestDTO;
import com.test.cards.entity.Application;
import com.test.cards.entity.RegisterOrder;
import com.test.cards.entity.RegisterUser;

public class ApplicationConverter {

	/**
	 * Сборка требуемой заявки для регистрации
	 * @param application пустой объект заявки
	 * @param requestDTO DTO с полученными параметрами
	 * @return application требуемая заявка для регистрации
	 */
	public static Application buildApplication(Application application, RequestDTO requestDTO) {
		if (application instanceof RegisterOrder) {
			application = buildRegisterOrder(application, requestDTO);

			return application;
		} else if (application instanceof RegisterUser) {
			application = buildRegisterUser(application, requestDTO);

			return application;
		} else {
			return null;
		}
	}

	/**
	 * Сборка заявки на регистрацию заказа
	 * @param application пустой объект заявки
	 * @param requestDTO DTO с полученными параметрами
	 * @return заявка на регистрацию заказа RegisterOrder
	 */
	private static RegisterOrder buildRegisterOrder(Application application, RequestDTO requestDTO){
		RegisterOrder registerOrder = (RegisterOrder) application;

		registerOrder.setDate(requestDTO.getDate());
		registerOrder.setPositions(requestDTO.getPositions());

		return registerOrder;
	}

	/**
	 * Сборка заявки на регистрацию пользователя
	 * @param application пустой объект заявки
	 * @param requestDTO DTO с полученными параметрами
	 * @return заявка на регистрацию пользователя RegisterUser
	 */
	private static RegisterUser buildRegisterUser(Application application, RequestDTO requestDTO) {
		RegisterUser registerUser = (RegisterUser) application;

		registerUser.setLogin(requestDTO.getLogin());
		registerUser.setPassword(requestDTO.getPassword());

		return registerUser;
	}
}
